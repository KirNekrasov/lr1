#include <iostream>
#include <string>
#include <sstream>
#include <utility>
#include <map>
#include <list>
#include <vector>
#include <algorithm>

using namespace std;

struct Command {
    char type;
    int value;
};

struct Coords {
    int state;
    string token;
};

bool operator<(const Coords &first, const Coords &second) {
    if (first.state == second.state) {
        return first.token < second.token;
    } else {
        return first.state < second.state;
    }
}

void addToTable(map<Coords, Command> &commandsTable, int state
        , const string &token, char type, int value) {
    commandsTable[Coords{ state, token }] = Command{ type, value };
}

map<Coords, Command> createCommandsTable() {
    map<Coords, Command> table;
    //Ok
    addToTable(table, 1, "O", 'O', 0);

    //Change state
    addToTable(table, 1, "E", 'S', 3);
    addToTable(table, 1, "Y", 'S', 4);
    addToTable(table, 1, "Z", 'S', 7);
    addToTable(table, 1, "S", 'S', 8);
    addToTable(table, 1, "p", 'S', 2);
    addToTable(table, 1, "iv", 'S', 12);

    addToTable(table, 4, "Z", 'S', 6);
    addToTable(table, 4, "S", 'S', 8);
    addToTable(table, 4, "B", 'S', 5);
    addToTable(table, 4, "p", 'S', 13);
    addToTable(table, 4, "iv", 'S', 12);

    addToTable(table, 8, "t", 'S', 9);

    addToTable(table, 9, "B", 'S', 10);
    addToTable(table, 9, "p", 'S', 13);

    addToTable(table, 10, "e", 'S', 11);

    //Reduct
    addToTable(table, 2, "$", 'R', 1);

    addToTable(table, 3, "$", 'R', 2);

    addToTable(table, 5, "$", 'R', 3);

    addToTable(table, 6, "p", 'R', 4);
    addToTable(table, 6, "iv", 'R', 4);

    addToTable(table, 7, "p", 'R', 5);
    addToTable(table, 7, "iv", 'R', 5);

    addToTable(table, 11, "p", 'R', 6);
    addToTable(table, 11, "iv", 'R', 6);

    addToTable(table, 12, "t", 'R', 7);

    addToTable(table, 13, "e", 'R', 8);
    addToTable(table, 13, "$", 'R', 8);

    return table;
}

struct Rule {
    char nonterm;
    int length;
};

vector<Rule> createRulesTable() {
    vector<Rule> rules(8);
    rules[0] = Rule{ 'O', 1 };
    rules[1] = Rule{ 'O', 1 };
    rules[2] = Rule{ 'E', 2 };
    rules[3] = Rule{ 'Y', 2 };
    rules[4] = Rule{ 'Y', 1 };
    rules[5] = Rule{ 'Z', 4 };
    rules[6] = Rule{ 'S', 1 };
    rules[7] = Rule{ 'B', 1 };
    
    return rules;
}

//Get tokens from the string
//Return empty string if error.
list<string> getTokens(const string &s) {
    auto tokens = list<string>{};
    if (!s.empty()) {
        for (auto it = s.begin(); it != s.end(); ++it) {
            auto token = string{ "" };
            auto firstLetters = { 'O', 'E', 'Y', 'B', 'Z'
                , 'S', 'i', 'p', 't', 'e' };
            if (any_of(firstLetters.begin()
                , firstLetters.end()
                , [&](char letter) { return letter == *it; })) {
                if (*it == 'i') {
                    if (++it != s.end()) {
                        if (*it == 'v') {
                            token = "iv";
                        } else {
                            tokens.clear();
                            break;
                        }
                    } else {
                        tokens.clear();
                        break;
                    }
                } else {
                    token = string{ *it };
                }
                tokens.push_back(token);
            } else {
                tokens.clear();
                break;
            }
        }
        if (!tokens.empty()) {
            tokens.push_back("$");
        }
    } else {
        tokens.push_back("$");
    }
    return tokens;
}

//Print info about stacks, and the string with the current position of cursor
void printInfo(const list<int> &stateStack, const list<string> &tokenStack
        , const list<string> &prefix, const list<string> &tokens) {
    cout << "#####################" << endl;
    cout << "State stack:";
    for (auto state : stateStack) {
        cout << state << ' ';
    }
    cout << endl;

    cout << "Token stack:";
    if (!tokenStack.empty()) {
        for (auto token : tokenStack) {
            cout << token << ' ';
        }
    }
    cout << endl;

    cout << "Parsed string state: ";
    for (auto &s : prefix) {
        cout << s;
    }
    cout << ">" << tokens.front() << "<";
    for (auto i = ++tokens.begin(); i != tokens.end(); ++i) {
        cout << *i;
    }
    cout << endl;
}

//Try to find the command in the table
bool tryFindInCommandsTable(const map<Coords, Command> &commandsTable
        , int currentState, const string &token, Command &nextCommand) {
    auto it = commandsTable.find(Coords{ currentState, token });
    if (it == commandsTable.end()) {
        return false;
    }

    nextCommand = Command{ it->second.type, it->second.value };
    return true;
}

//Shift
void shift(const Command &nextCommand, list<int> &stateStack, list<string> &tokenStack
        , list<string> &prefix, list<string> &tokens, bool isVirtual) {
    cout << "Do " << nextCommand.type << nextCommand.value << endl;
    tokenStack.push_back(tokens.front());
    if (!isVirtual) {
        prefix.push_back(tokens.front());
    }
    tokens.pop_front();
    stateStack.push_back(nextCommand.value);
}

//Reduction
void reduction(const Command &nextCommand, list<int> &stateStack, list<string> &tokenStack
        , const vector<Rule> &rules, list<string> &tokens) {
    cout << "Do " << nextCommand.type << nextCommand.value << endl;
    for (auto i = 0; i < rules[nextCommand.value - 1].length; ++i) {
        stateStack.pop_back();
        tokenStack.pop_back();
    }

    //Add "virtual" nonterm
    tokens.push_front(string{ rules[nextCommand.value - 1].nonterm });
}

//Parse the string
//Return true if the string is correct, otherwise false
bool tryParseString(const string &stringToParse) {
    auto commandsTable = createCommandsTable();
    auto rules = createRulesTable();
    auto stateStack = list < int > { 1 };
    auto tokenStack = list < string > {};
    auto isReadingVirtual = false;

    //Try to get tokens from the string
    auto tokens = getTokens(stringToParse);
    if (tokens.empty()) {
        cout << "Undefined token in the string." << endl;
        return false;
    }
    auto prefix = list < string > {};

    while (true) {
        //Print info
        if (tokens.empty()) {
            return false;
        }

        printInfo(stateStack, tokenStack, prefix, tokens);

        //Try to find the command in the table
        auto nextCommand = Command{};
        if (!tryFindInCommandsTable(commandsTable, stateStack.back(), tokens.front(), nextCommand)) {
            cout << "Cant get the next command." << endl;
            return false;
        }

        if (nextCommand.type == 'S') {
            //Change state
            if (isReadingVirtual) {
                cout << "Imitation of reading nonterm." << endl;
            }
            shift(nextCommand, stateStack, tokenStack, prefix, tokens, isReadingVirtual);
            isReadingVirtual = false;
        } else if (nextCommand.type == 'R') {
            //Reduction
            //Has rule?
            if (nextCommand.value > rules.size()) {
                cout << "Error in the commands table. Undefined rule." << endl;
                return false;
            }

            //Can pop values?
            if ((rules[nextCommand.value - 1].length > tokenStack.size())
                    || ((rules[nextCommand.value - 1].length + 1) > stateStack.size())) {
                return false;
            }

            //Do it
            reduction(nextCommand, stateStack, tokenStack, rules, tokens);
            isReadingVirtual = true;
        } else if (nextCommand.type == 'O') {
            tokens.pop_front();
            cout << "Axiom and the initial state => out, ";
            //OK?
            if ((stateStack.size() != 1) || (stateStack.front() != 1)
                    || (!tokenStack.empty()) || (tokens.size() != 1)
                    || (tokens.front() != "$")) {
                cout << "but, parsing is not over." << endl;
                return false;
            }
            cout << "the string is over and stacks are in the correct states." << endl;
            return true;
        } else {
            cout << "Error in the parse table. Undefined command." << endl;
            return false;
        }
    }
}

int main() {
    string toParse;
    cout << "Enter string (DO NOT USE '$' IN STRING): ";
    getline(cin, toParse);

    auto isOk = tryParseString(toParse);

    cout << "String: \"" << toParse << "\" is " << (isOk ? "" : " not ") << "correct." << endl;

    cout << "Press any key to continue..." << endl;
    getchar();
    return 0;
}